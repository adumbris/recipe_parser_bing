import json
import fire


class URL2Links:
    def __init__(self, jsonfile, linksname="links.txt", metadataname="linksmeta.json"):
        self.data = self.readjson(jsonfile)
        self.linksname = linksname
        self.metadataname = metadataname

    def readjson(self, jsonfile):
        data = {}
        with open(jsonfile) as f:
            data = json.load(f)
        return data

    def print(self, limit=2):
        for k,v in self.data.items():
            print(k)
            for k2,v2 in v.items():
                print(k2, v2)
            print("---")

    def process(self):
        links_fh = open(self.linksname, "w")
        meta_fh = open(self.metadataname, "w")
        i = 1
        for k,v in self.data.items():
            d = {}
            j = 1
            d["name"] = k
            d["id"] = "{0:06d}".format(i)
            d["id2"] = i
            for k2,v2 in v.items():
                if k2 == 'urls':
                    for url in v2:
                        links_fh.write("{0}\n  out={1:06d}_{2:06d}.html\n".format(url, i, j))
                        j += 1
                else:
                    d[k2] = v2
            meta_fh.write("{}\n".format(json.dumps(d)))
            i += 1

        links_fh.close()
        meta_fh.close()



if __name__ == '__main__':
    fire.Fire(URL2Links)
