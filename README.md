# Extracting recipe data from bing search results

To speed up an execution time I am using a combination of console tools: aria2c - parallel downloader, html-xml-utils - html parser, jq - json processor

* `bin` directory contains executable scripts
* `data` directory contains links data and py script export it from Bing format.


## Install prerequments on Ubuntu
```
sudo apt install aria2 html-xml-utils jq

```

### Start

```
git clone [url]
cd [reponame]
#Make sure you have right links.txt file in data dir
./run_all.sh
```
