#!/bin/bash
OUTDIR="/media/data/recipes_json__2"
NAME=$(basename -a -s .json $1)
JSON="$(cat ${1} | jq --arg name "$NAME" -c '{data: ., name: $name | capture("[0]+(?<id2>[0-9]+)_[0]+(?<order>[0-9]+)")}')"

if [[ "$JSON" ]]; then
       echo "$JSON" > "${OUTDIR}/${NAME}.json"
fi

