#!/bin/bash
OUTDIR="/media/data/recipes_json"
NAME=$(basename -a -s .html $1)
JSON="$(cat ${1} | hxclean | hxnormalize -x | hxselect -s "\n" -c 'script[type="application/ld+json"]' | hxuncdata | jq -c .)"

if [[ "$JSON" ]]; then
       echo "$JSON" > "${OUTDIR}/${NAME}.json"
fi
