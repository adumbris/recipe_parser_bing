#!/bin/sh

BASEDIR="/media/data"

ria2c -i ../data/links.txt -d "${BASEDIR}/recipes_htmls" -c --auto-file-renaming=false -m 2 -t 10 --no-conf=false --conf-path=aria2.conf --save-session=session --save-session-interval=60 -j 300
find "${BASEDIR}/recipes_htmls" -type f -name '*.html' | xargs -n1 -P 8 ./extract_json.sh
find "${BASEDIR}/recipes_json" -type f -name '*.json' | xargs -n1 -P 8 ./process_json.sh
find "${BASEDIR}/recipes_json__2" -type f -name '*.json' -exec cat {} + >> "${BASEDIR}/recipes_output.json"

cat "${BASEDIR}/recipes_output.json" | jq -c 'select(.data."@type"=="Recipe")' | jq -c .data > jq -c ${BASEDIR}/BN1.json
